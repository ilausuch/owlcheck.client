<?php

echo "owlcheck ".file_get_contents("version")."\n";

function getParams($argv,$list){
	$values=array();
	
	for ($i=1; $i<count($argv); $i++){
		$res = explode('=', $argv[$i]);
		if (count($res)!=2)
			die("Usage: php -f {$argv[0]} group=#group# key=#key# secret=#secret#\n");
			
		$values[$res[0]]=$res[1];
	}
	
	foreach ($list as $item){
		if (!isset($values[$item]))
			die("Usage: php -f {$argv[0]} group=#group# key=#key# secret=#secret#\n");
	}
	
	return $values;
}


$params=getParams($argv,array("group","key","secret"));

$ch = curl_init();
$url="https://owlcheck.com/api/v1/group/{$params["group"]}/sensors";
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_HTTPAUTH,  CURLAUTH_BASIC);
curl_setopt($ch, CURLOPT_USERPWD, "{$params["key"]}:{$params["secret"]}"); 
$res=curl_exec($ch);
curl_close($ch);

$res=json_decode($res,true);

if ($res==null || $res["success"]==0)
	die("Invalid group");
	
$res=$res["data"];

$sensors=$res;

foreach ($sensors as &$sensor){
	$sensor["next"]=time();
}

while(1){
	foreach ($sensors as &$sensor){
		if ($sensor["next"]<time()){
			$output=null;
			$exec="php -f owlcheck.php sensor={$sensor["accessKey"]} key={$params["key"]} secret={$params["secret"]}\n";
			echo $exec;
			exec($exec,$output);
			
			foreach ($output as $out)
				echo $out."\n";
				
			echo "\n";
			
			$sensor["next"]=time()+$sensor["frequency"];	
		}
	}	
	
	sleep(1);
}