<?php

echo "owlcheck ".file_get_contents("version")."\n";

function getError($error){
	return array(
		"status"=>"UNKNOWN",
		"value"=>0,
		"detail"=>$error
	);	
}

function trimArray($list){
	$values=array();
	foreach ($list as $o){
		if (trim($o)!="")
			$values[]=$o;
	}
	
	return $values;
}

function changeKeys($list,$keys){
	$kc=0;
	$list2=array();
	foreach ($list as $v){
		if (isset($keys[$kc])){
			$list2[$keys[$kc]]=$v;
			$kc++;
		}
	}
	
	return $list2;
}

function exec2array($exec,$keys){
	$out=exec($exec);
	$out=explode(" ",trim($out));
	
	return changeKeys(trimArray($out),$keys);
}

function getParams($argv,$list){
	$values=array();
	
	for ($i=1; $i<count($argv); $i++){
		$res = explode('=', $argv[$i]);
		if (count($res)!=2)
			die("Usage: php -f {$argv[0]} sensor=#group# key=#key# secret=#secret#\n");
			
		$values[$res[0]]=$res[1];
	}
	
	foreach ($list as $item){
		if (!isset($values[$item]))
			die("Usage: php -f {$argv[0]} sensor=#group# key=#key# secret=#secret#\n");
	}
	
	return $values;
}



function mem($sensor){
	$out1=exec2array("free -m|grep Mem:",array("label","total","used","free","shared","buffers","cached"));
	$out2=exec2array("free -m|grep buffers/cache",array("label1","label2","used","free"));
	unset($out1["label"]);
	$out1["real_used"]=$out2["used"];
	$out1["real_free"]=$out2["free"];
	$out1["real_useP"]=100*$out2["used"]/$out1["total"];
	
	$res=$out1;
	
	$status="OK";

	if ($res["real_useP"]>=$sensor["warningLimit"])
		$status="WARNING";
		
	if ($res["real_useP"]>=$sensor["criticalLimit"])
		$status="CRITICAL";
		
	return array(
		"status"=>$status,
		"value"=>$res["real_useP"],
		"detail"=>"Used {$res["real_used"]} mb. of {$res["total"]} mb. Free {$res["real_free"]} mb."
	);
}

function swap($sensor){
	$res=exec2array("free -m|grep Swap:",array("label","total","used","free"));
	$res["useP"]=100*$res["used"]/$res["total"];

	$status="OK";

	if ($res["useP"]>=$sensor["warningLimit"])
		$status="WARNING";
		
	if ($res["useP"]>=$sensor["criticalLimit"])
		$status="CRITICAL";
		
	return array(
		"status"=>$status,
		"value"=>$res["useP"],
		"detail"=>"Used {$res["used"]} mb. of {$res["total"]} mb. Free {$res["free"]} mb."
	);
}

function disk($sensor){
	if (!isset($sensor["config"]["device"]))
		return getError("Disk exec needs device config");
		
	$res=exec2array("df -h | grep \"{$sensor["config"]["device"]}\"",array("dev","size","used","free","useP","mounted_on"));

	$res["useP"]=str_replace("%","",$res["useP"]);
	
	$status="OK";
	
	if ($res["useP"]>=$sensor["warningLimit"])
		$status="WARNING";
		
	if ($res["useP"]>=$sensor["criticalLimit"])
		$status="CRITICAL";

	return array(
		"status"=>$status,
		"value"=>$res["useP"],
		"detail"=>"Used {$res["used"]} of {$res["size"]}. Free {$res["free"]}"
	);
}

function cpu($sensor){
	if (isset($sensor["config"]["load"])){
		switch($sensor["config"]["load"]){
			case "1":
				$load="load1";
			break;
			case "5":
				$load="load5";
			break;
			case "15":
				$load="load15";
			break;
			default:
				return getError("Invalid load config");
		}
	}
	else
		$load="load1";
		
		
	$res=exec("uptime");
	$res=str_replace("load average:","",$res);
	$res=explode(",",$res);
	foreach ($res as &$item)
		$item=trim($item);
	
	$res["load1"]=$res[3];
	$res["load5"]=$res[4];
	$res["load15"]=$res[5];	
	
	$status="OK";

	if ($res["load1"]>=$sensor["warningLimit"])
		$status="WARNING";
		
	if ($res["load1"]>=$sensor["criticalLimit"])
		$status="CRITICAL";
		
	return array(
		"status"=>$status,
		"value"=>$res[$load],
		"detail"=>"Loads {$res["load1"]}, {$res["load5"]}, {$res["load15"]}"
	);		

}

function reg($sensor){
	if (!isset($sensor["config"]["file"]))
		return getError("Reg needs file config");
	
	if (!isset($sensor["config"]["var"]))
		return getError("Reg needs var config");	

	if (!file_exists($sensor["config"]["file"]))	
		return getError("File {$sensor["config"]["file"]} doesn't exist");
			
	$content=file_get_contents($sensor["config"]["file"]);
	
	$lines=explode("\n",$content);
	foreach ($lines as $line){
		$varValue=explode("=",$line);
		if (count($varValue)==2){
			if ($varValue[0]==$sensor["config"]["var"]){
				
				$value=$varValue[1];
				$status="OK";
				
				if ($value>=$sensor["warningLimit"])
					$status="WARNING";
					
				if ($value>=$sensor["criticalLimit"])
					$status="CRITICAL";
				
				return array(
					"status"=>$status,
					"value"=>$value,
					"detail"=>"Value $value"
				);	
			}
				
		}
	}
}

function json_($sensor){
	if (!isset($sensor["config"]["file"]))
		return getError("Reg needs file config");
	
	if (!isset($sensor["config"]["var"]))
		return getError("Reg needs var config");	

	if (!file_exists($sensor["config"]["file"]))	
		return getError("File {$sensor["config"]["file"]} doesn't exist");
			
	$content=file_get_contents($sensor["config"]["file"]);
	
	
	$json=json_decode($content,true);
	if ($json=="")
		return getError("Invalid json");
		
	$item=$json;
	$path=explode(".",$sensor["config"]["var"]);
	foreach ($path as $pathItem){
		if (!isset($item[$pathItem]))
			return getError("Cannot access to $pathItem in json");
			
		$item=$item[$pathItem];
	}
	
	if (is_array($item))
		return getError("Selected item is an array");
		
	
	$value=$item;
	$status="OK";
	
	if ($value>=$sensor["warningLimit"])
		$status="WARNING";
		
	if ($value>=$sensor["criticalLimit"])
		$status="CRITICAL";
	
	return array(
		"status"=>$status,
		"value"=>$value,
		"detail"=>"Value $value"
	);
}

function executeOnce($params){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://owlcheck.com/api/v1/sensor/{$params["sensor"]}/conf");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_USERPWD, "{$params["key"]}:{$params["secret"]}");  
	$res=curl_exec($ch);
	curl_close($ch);
	
	$res=json_decode($res,true);
	if (!$res["success"])
		die("Cannot get sensor info\n");
		
	$sensor=$res["data"];
	
	if ($sensor["type"]!="auto")
		die("This sensor is not auto\n");
	
	$configs=array();
	if (trim($sensor["config"])!=""){
		$config=explode(";",trim($sensor["config"]));
		foreach ($config as $keyValue){
			$keyValue=explode("=",$keyValue);
			if (count($keyValue)!=2)
				die("Invalid Keyvalue '{$keyValue}' in config");
			
			$configs[$keyValue[0]]=$keyValue[1];	
		}
		
	}
	
	$sensor["config"]=$configs;
		
	switch($sensor["exec"]){
		case "mem":
			$res=mem($sensor);
		break;
		case "swap":
			$res=swap($sensor);
		break;
		case "cpu":
			$res=cpu($sensor);
		break;
		case "disk":
			$res=disk($sensor);
		break;
		case "reg":
			$res=reg($sensor);
		break;
		case "json":
			$res=json_($sensor);
		break;
		default:
			die("Invalid exec {$sensor["exec"]}\n");
	}
	
	
	
	//---------------------------------------------------------------------------------
	$detail=base64_encode($res["detail"]);
	$url="https://owlcheck.com/api/v1/sensor/{$params["sensor"]}/check/put/?status={$res["status"]}&value={$res["value"]}&detail={$detail}";
	
	echo "-----------------------------------------------------------------------------\n";
	echo $url."\n";
	print_r($res);
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_HTTPAUTH,  CURLAUTH_BASIC);
	curl_setopt($ch, CURLOPT_USERPWD, "{$params["key"]}:{$params["secret"]}");
	curl_exec($ch);
	curl_close($ch);
	echo "\n";
}

//---------------------------------------------------------------------------------

$params=getParams($argv,array("sensor","key","secret"));

executeOnce($params);
