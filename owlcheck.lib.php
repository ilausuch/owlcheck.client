<?php

class OwlcheckBase{
	protected $key;
	protected $secret;
	protected $accessKey;
	
	protected function __construct($key,$secret,$accessKey,$autoLoad){
		$this->key=$key;
		$this->secret=$secret;	
		$this->accessKey=$accessKey;
		
		if ($autoLoad)
			$this->load();
	}
	
	public function load(){
		
	}
	
	protected function call($url){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HTTPAUTH,  CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_USERPWD, "{$this->key}:{$this->secret}"); 
		$ret=curl_exec($ch);
		curl_close($ch);
		
		return $ret;
	}
	
	protected function callSuccess($url,$errorMessage){
		$result=$this->call($url);
		
		$res=json_decode($result,true);
		if (!$res["success"])
			if (isset($res["error"]))
				throw new Exception("{$errorMessage}, cause: {$res["error"]}\n");
			else
				throw new Exception("{$errorMessage}, cause: {$result}\n");
		
		if (isset($res["data"]))
			return $res["data"];
		else
			return $res;
	}
}

//-----------------------------------------------------------------------------
//! Owlcheck
//-----------------------------------------------------------------------------
class Owlcheck extends OwlcheckBase{
	public $groups;
	
	public function __construct($key,$secret,$autoLoad=true){
		parent::__construct($key,$secret,"",$autoLoad);
	}
	
	public function load(){
		$url="https://owlcheck.com/api/v1/group/all";
		$groups=$this->callSuccess($url,"Cannot get groups");
		
		$this->groups=array();
		foreach ($groups as $group){
			$newGroup=new OwlcheckGroup($this->key,$this->secret,$group["accessKey"],true);
			$newGroup->data=$group;
			$this->groups[$group["accessKey"]]=$newGroup;
		}
		
		return $this->groups;
	}
	
	public function getGroupByName($groupName){
		foreach ($this->groups as $group)
			if ($group->data["groupName"]==$groupName)
				return $group;
				
		throw new Exception("Cannot find group $groupName");
	}
	
	public function getSensorByName($groupName,$sensorName){
		$group=$this->getGroupByName($groupName);
		return $group->getSensorByName($sensorName);
	}
	
	public function getNextChecks(){
		$res=array();
		foreach ($this->groups as $group)
			$res=array_merge($res,$group->getNextChecks());
			
		return $res;
	}
}

//-----------------------------------------------------------------------------
//! Group
//-----------------------------------------------------------------------------
class OwlcheckGroup extends OwlcheckBase{
	public $data;
	public $sensors;
	
		
	public function __construct($key,$secret,$accessKey,$autoLoad=true){
		parent::__construct($key,$secret,$accessKey,$autoLoad);
	}
	
	public function load(){
		$url="https://owlcheck.com/api/v1/group/{$this->accessKey}/sensors";
		$sensors=$this->callSuccess($url,"Cannot get group sensors");
		
		$this->sensors=array();
		foreach ($sensors as $sensor){
			$newSensor=new OwlcheckSensor($this->key,$this->secret,$sensor["accessKey"],false);
			$newSensor->data=$sensor;
			$newSensor->group=$this;
			$newSensor->prepareConfig();
			$this->sensors[$sensor["accessKey"]]=$newSensor;
		}
		
		return $this->sensors;	
	}
	
	public function getSensorByName($sensorName){
		foreach ($this->sensors as $sensor)
			if ($sensor->data["name"]==$sensorName)
				return $sensor;
				
		throw new Exception("Cannot find sensor $sensorName");
	}
	
	public function getNextChecks(){
		$res=array();
		foreach ($this->sensors as $sensor)
			if ($sensor->needsBeChecked())
				$res[]=$sensor;
			
		return $res;
	}
	
	public function name(){
		return $this->data["groupName"];
	}
}

//-----------------------------------------------------------------------------
//! Sensor
//-----------------------------------------------------------------------------
class OwlcheckSensor extends OwlcheckBase{
	public $data;
	public $nextTimestamp;
	public $group;
	
	public function __construct($key,$secret,$accessKey,$autoLoad=true){
		parent::__construct($key,$secret,$accessKey,$autoLoad);
		$this->nextTimestamp=0;
	}
	
	public function load(){
		$url="https://owlcheck.com/api/v1/sensor/{$this->accessKey}/conf"; 
		$sensor=$this->callSuccess($url,"Cannot get group sensors");
		$this->data=$sensor;
		
		$this->prepareConfig();
		
		return $this->data;
	}
	
	public function prepareConfig(){
		$configs=array();
		if (trim($this->config())!=""){
			$config=explode(";",trim($this->config()));
			foreach ($config as $keyValue){
				$keyValue=explode("=",$keyValue);
				if (count($keyValue)!=2)
					throw new Exception("Invalid Keyvalue '{$keyValue}' in config");
				
				$configs[$keyValue[0]]=$keyValue[1];	
			}
		}
		
		$this->data["config"]=$configs;
	}
	
	public function setCheck($status,$value,$detail){
		$detail=base64_encode($detail);
		$url="https://owlcheck.com/api/v1/sensor/{$this->accessKey}/check/put/?status={$status}&value={$value}&detail={$detail}";
		return $this->callSuccess($url,"Cannot set key value");
	}
	
	public function computeCheck($value,$detail){
		if ($this->high_is_worst()){
			if ($value>=$this->criticalLimit())
				return $this->setCheck("CRITICAL",$value,$detail);
			else
				if ($value>=$this->warningLimit())
					return $this->setCheck("WARNING",$value,$detail);
				else
					return $this->setCheck("OK",$value,$detail);
		}
		else{
			if ($value<=$this->criticalLimit())
				return $this->setCheck("CRITICAL",$value,$detail);
			else
				if ($value<=$this->warningLimit())
					return $this->setCheck("WARNING",$value,$detail);
				else
					return $this->setCheck("OK",$value,$detail);
		}
	}
	
	public function name(){
		return $this->data["name"];
	}
	
	public function type(){
		return $this->data["type"];
	}
	
	public function exec(){
		return $this->data["exec"];
	}
	
	public function frequency(){
		return $this->data["frequency"];
	}
	
	public function warningLimit(){
		return $this->data["warningLimit"];
	}
	
	public function criticalLimit(){
		return $this->data["criticalLimit"];
	}
	
	public function high_is_worst(){
		return $this->data["frequency"];
	}
	
	public function config($key=""){
		if ($key=="")
			return $this->data["config"];
		else
			if (isset($this->data["config"][$key]))
				return $this->data["config"][$key];
			else
				throw new Exception("$key not founded in configuration");
	}
	
	
	public function updateTimestamp(){
		$this->nextTimestamp=time()+$this->frequency();
	}
	
	public function needsBeChecked(){
		if (time()>$this->nextTimestamp){
			$this->updateTimestamp();
			return true;
		}
		else
			return false;
	}
}